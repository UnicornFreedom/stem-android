package totoro.stem

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import totoro.stem.databinding.ActivityChannelBinding

class ChannelActivity : AppCompatActivity() {
    private lateinit var binding: ActivityChannelBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityChannelBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}
