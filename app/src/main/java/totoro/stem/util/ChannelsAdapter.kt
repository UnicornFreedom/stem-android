package totoro.stem.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import totoro.stem.R

class ChannelsAdapter(private val channels : List<String>) : RecyclerView.Adapter<ChannelsAdapter.ViewHolder>() {

    lateinit var callback: Callback

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_channel, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.text.text = channels[position]
        holder.itemView.setOnClickListener { callback.onClick(position) }
    }

    override fun getItemCount(): Int {
        return channels.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val text: TextView = itemView.findViewById(R.id.text_channel_label)
    }

    fun setOnTouchCallback(callback :Callback) {
        this.callback = callback
    }

    interface Callback {
        fun onClick(position :Int)
    }
}