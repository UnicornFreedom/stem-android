package totoro.stem.util

import android.content.Context

class SharedPref {
    companion object {
        fun addChannel(context : Context, channel : String) {
            val sp = context.getSharedPreferences("savedChannels", Context.MODE_PRIVATE)
            val currentChannels = sp.getStringSet("channels", HashSet<String>())
            val newChannelsSet = currentChannels?.let { HashSet<String>(it) }
            newChannelsSet?.add(channel)
            sp.edit().clear().putStringSet("channels", newChannelsSet).apply()
        }

        fun getChannels(context : Context) : List<String> {
            val sp = context.getSharedPreferences("savedChannels", Context.MODE_PRIVATE)
            return sp.getStringSet("channels", HashSet<String>())?.toList() ?: ArrayList()
        }
    }
}