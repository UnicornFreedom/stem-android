package totoro.stem

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import totoro.stem.databinding.ActivityMainBinding
import totoro.stem.util.ChannelsAdapter
import totoro.stem.util.SharedPref

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.buttonAdd.setOnClickListener { showJoinPopupWindow(it) }
    }

    private fun showJoinPopupWindow(view: View) {
        val inflater = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView = inflater.inflate(R.layout.popup_join_channel, null)

        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT

        val popupWindow = PopupWindow(popupView, width, height, true)

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window token
        popupWindow.animationStyle = R.style.PopupAnimation
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
        popupWindow.dimBehind()

        val buttonClose = popupView.findViewById<ImageButton>(R.id.button_popup_join_channel_close)
        buttonClose.setOnClickListener {
            popupWindow.dismiss()
        }
    }

    override fun onResume() {
        super.onResume()
        val ch = SharedPref.getChannels(this)
        fillRecycler(ch)
    }

    private fun fillRecycler(channels : List<String>) {
        binding.recyclerChannels.apply {
            this.layoutManager = LinearLayoutManager(context)
            val adapter = ChannelsAdapter(channels)
            adapter.setOnTouchCallback(object : ChannelsAdapter.Callback {
                override fun onClick(position: Int) {
                    startActivity(Intent(context, ChannelActivity::class.java))
                }
            })
            this.adapter = adapter
        }
    }
}
